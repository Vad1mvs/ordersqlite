package com.lesson.vadim.prosqlite.spinnerpackage;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.lesson.vadim.prosqlite.PriceDbHelper;

import java.util.ArrayList;


public class SpinnerDbAdapter {

    public static final String C_TABLE = "SPINNER" ;
    public static final String C_ID = "_id";
    public static final String C_NUMBER = "spinner_num";

    private Context context;
    private PriceDbHelper dbHelper;
    private SQLiteDatabase db;

    private String[] column = new String[]{
            C_ID,
            C_NUMBER
    } ;

    public SpinnerDbAdapter(Context context)
    {
        this.context = context;
    }

    public SpinnerDbAdapter abrir() throws SQLException
    {
        dbHelper = new PriceDbHelper(context);
        db = dbHelper.getWritableDatabase();
        return this;
    }

    public void cerrar()
    {
        dbHelper.close();
    }
    public Cursor getRegistro(long id) throws SQLException
    {
        if (db == null)
            abrir();

        Cursor c = db.query( true, C_TABLE, column, C_ID + "=" + id, null, null, null, null, null);
        if (c != null) {
            c.moveToFirst();
        }
        return c;
    }

    public ArrayList<SpinnerConst> getSpinner(String filter)
    {
        ArrayList<SpinnerConst> spinnerConsts = new ArrayList<SpinnerConst>();

        if (db == null)
            abrir();

        Cursor c = db.query(true, C_TABLE, column, filter, null, null, null, null, null);

        for(c.moveToFirst(); !c.isAfterLast(); c.moveToNext())
        {
            spinnerConsts.add(SpinnerConst.cursorToSpinnerConst(context, c));
        }

        c.close();

        return spinnerConsts;
    }
}