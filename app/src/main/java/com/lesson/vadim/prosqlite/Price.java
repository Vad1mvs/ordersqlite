package com.lesson.vadim.prosqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

public class Price {

    private Context context;
    private Long id;
    private String number;
    private String color;
    private String note;
    private String email;
    private String phone;
    private String quantity1;
    private String quantity2;
    private String date;
    private Long spinnerId;
    private Long spinnerId_2;

    private String count;

    public Price(Context context)
    {
        this.context = context;
    }

    public Price(Context context, Long id, String number, String color, String note, String email,
                 String phone, String quantity1, String quantity2, String date, Long spinnerId, Long spinnerId_2, String count) {
        this.context = context;
        this.id = id;
        this.number = number;
        this.color = color;
        this.note = note;
        this.email = email;
        this.phone = phone;
        this.quantity1 = quantity1;
        this.quantity2 = quantity2;
        this.date = date;
        this.spinnerId = spinnerId;
        this.spinnerId_2 = spinnerId_2;
        this.count = count;
    }


    @Override
    public String toString() {
        return "Date: " + date ;
    }

    public String getQuantity1() {
        return quantity1;
    }

    public void setQuantity1(String quantity1) {
        this.quantity1 = quantity1;
    }

    public String getQuantity2() {
        return quantity2;
    }

    public void setQuantity2(String quantity2) {
        this.quantity2 = quantity2;
    }

    public Long getSpinnerId_2() {
        return spinnerId_2;
    }

    public void setSpinnerId_2(Long spinnerId_2) {
        this.spinnerId_2 = spinnerId_2;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getSpinnerId() {
        return spinnerId;
    }

    public void setSpinnerId(Long spinnerId) {
        this.spinnerId = spinnerId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public static Price find(Context context, long id)
    {
        PriceDbAdapter dbAdapter = new PriceDbAdapter(context);
        Cursor c = dbAdapter.getRegister(id);
        Price price = Price.cursorToPrice(context, c);
        c.close();
        return price;
    }

    public static Price cursorToPrice(Context context, Cursor c)
    {
        Price price = null;

        if (c != null)
        {
            price = new Price(context);

            price.setId(c.getLong(c.getColumnIndex(PriceDbAdapter._ID)));
            price.setNumber(c.getString(c.getColumnIndex(PriceDbAdapter.NUMBER)));
            price.setColor(c.getString(c.getColumnIndex(PriceDbAdapter.COLOR)));
            price.setNote(c.getString(c.getColumnIndex(PriceDbAdapter.NOTE)));
            price.setEmail(c.getString(c.getColumnIndex(PriceDbAdapter.EMAIL)));
            price.setPhone(c.getString(c.getColumnIndex(PriceDbAdapter.PHONE)));
            price.setQuantity1(c.getString(c.getColumnIndex(PriceDbAdapter.QUANTITY1)));
            price.setQuantity2(c.getString(c.getColumnIndex(PriceDbAdapter.QUANTITY2)));
            price.setSpinnerId(c.getLong(c.getColumnIndex(PriceDbAdapter.C_SPINNER)));
            price.setSpinnerId_2(c.getLong(c.getColumnIndex(PriceDbAdapter.C_SPINNER2)));
            price.setCount(c.getString(c.getColumnIndex(PriceDbAdapter.COUNT)));
            price.setDate(c.getString(c.getColumnIndex(PriceDbAdapter.DATE)));
        }

        return price ;
    }

    private ContentValues toContentValues()
    {
        ContentValues reg = new ContentValues();

        reg.put(PriceDbAdapter._ID, this.getId());
        reg.put(PriceDbAdapter.NUMBER, this.getNumber());
        reg.put(PriceDbAdapter.COLOR, this.getColor());
        reg.put(PriceDbAdapter.NOTE, this.getNote());
        reg.put(PriceDbAdapter.EMAIL, this.getEmail());
        reg.put(PriceDbAdapter.PHONE, this.getPhone());
        reg.put(PriceDbAdapter.QUANTITY1, this.getQuantity1());
        reg.put(PriceDbAdapter.QUANTITY2, this.getQuantity2());
        reg.put(PriceDbAdapter.C_SPINNER, this.getSpinnerId());
        reg.put(PriceDbAdapter.C_SPINNER2, this.getSpinnerId_2());
        reg.put(PriceDbAdapter.COUNT, this.getCount());
        reg.put(PriceDbAdapter.DATE, this.getDate());

        return reg;
    }

    public long save()
    {
        PriceDbAdapter dbAdapter = new PriceDbAdapter(this.getContext());
        if ((this.getId() == null) || (!dbAdapter.exists(this.getId()))) {
            long newId = dbAdapter.insert(this.toContentValues());
            if (newId != -1) {
                this.setId(newId);
            }
        }
        else {
            dbAdapter.update(this.toContentValues());
        }
        return this.getId();
    }

    public long delete()
    {
        PriceDbAdapter dbAdapter = new PriceDbAdapter(this.getContext());
        return dbAdapter.delete(this.getId());
    }
}
