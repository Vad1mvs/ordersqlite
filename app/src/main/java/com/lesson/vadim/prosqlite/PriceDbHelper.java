package com.lesson.vadim.prosqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class PriceDbHelper extends SQLiteOpenHelper {

    private static int version = 4;
    private static String name = "ordersDb" ;
    private static CursorFactory factory = null;

    public PriceDbHelper(Context context)
    {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL( "CREATE TABLE TABLE1(" +
                " _id INTEGER PRIMARY KEY," +
                " number TEXT NOT NULL, " +
                " color TEXT, " +
                " note TEXT, " +
                " email TEXT, " +
                " phone TEXT, " +
                " date TEXT, " +
                " count TEXT, " +
                " quantity1 TEXT, " +
                " quantity2 TEXT);" );

        db.execSQL( "CREATE UNIQUE INDEX number ON TABLE1(number ASC)" );

        upgrade_2(db);
        upgrade_3(db);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        if (oldVersion < 2)
        {
            upgrade_2(db);
        }
        if (oldVersion < 3)
        {
            upgrade_3(db);
        }
    }

    private void upgrade_2(SQLiteDatabase db)
    {
        db.execSQL( "CREATE TABLE SPINNER2(" + " _id INTEGER PRIMARY KEY," + " spinner_num_2 TEXT NOT NULL)");
        db.execSQL( "CREATE UNIQUE INDEX spinner_num_2 ON SPINNER2(spinner_num_2 ASC)" );

        db.execSQL("INSERT INTO SPINNER2(_id, spinner_num_2) VALUES(1,'Milan')");
        db.execSQL("INSERT INTO SPINNER2(_id, spinner_num_2) VALUES(2,'Modern')");
        db.execSQL("INSERT INTO SPINNER2(_id, spinner_num_2) VALUES(3,'Alter')");
        db.execSQL("INSERT INTO SPINNER2(_id, spinner_num_2) VALUES(4,'Siena')");

        db.execSQL("ALTER TABLE TABLE1 ADD spinner_id_2 INTEGER NOT NULL DEFAULT 1");
    }
    private void upgrade_3(SQLiteDatabase db)
    {
        db.execSQL( "CREATE TABLE SPINNER(" + " _id INTEGER PRIMARY KEY," + " spinner_num TEXT NOT NULL)");
        db.execSQL( "CREATE UNIQUE INDEX spinner_num ON SPINNER(spinner_num ASC)" );

        db.execSQL("INSERT INTO SPINNER(_id, spinner_num) VALUES(1,'Parma')");
        db.execSQL("INSERT INTO SPINNER(_id, spinner_num) VALUES(2,'Parma L')");
        db.execSQL("INSERT INTO SPINNER(_id, spinner_num) VALUES(3,'Malevich')");
        db.execSQL("INSERT INTO SPINNER(_id, spinner_num) VALUES(4,'Plastic')");

        db.execSQL("ALTER TABLE TABLE1 ADD spinner_id INTEGER NOT NULL DEFAULT 1");

    }

}
