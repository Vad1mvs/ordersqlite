package com.lesson.vadim.prosqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;


public class PriceDbAdapter {

    public static final String TABLE = "TABLE1" ;
    public static final String _ID = "_id";
    public static final String NUMBER = "number";
    public static final String COLOR = "color";
    public static final String NOTE = "note";
    public static final String EMAIL = "email";
    public static final String PHONE = "phone";
    public static final String DATE = "date";
    public static final String QUANTITY1 = "quantity1";
    public static final String QUANTITY2 = "quantity2";
    public static final String C_SPINNER = "spinner_id";
    public static final String C_SPINNER2 = "spinner_id_2";

    public static final String COUNT = "count";


    private Context context;
    private PriceDbHelper dbHelper;
    private SQLiteDatabase db;

    private String[] colums = new String[]{
            _ID,
            NUMBER,
            COLOR,
            NOTE,
            EMAIL,
            PHONE,
            QUANTITY1,
            QUANTITY2,
            C_SPINNER,
            C_SPINNER2,
            COUNT,
            DATE} ;

    public PriceDbAdapter(Context context)
    {
        this.context = context;
    }

    public PriceDbAdapter open() throws SQLException
    {
        dbHelper = new PriceDbHelper(context);
        db = dbHelper.getWritableDatabase();
        return this;
    }

    public void close()
    {
        dbHelper.close();
    }


    public Cursor getCursor(String filter) throws SQLException {

        if (db == null)
            open();
        Cursor c = db.query(true, TABLE, colums, filter, null, null, null, null, null);

        return c;
    }
    public Cursor getRegister(long id) throws SQLException
    {
        if (db == null)
            open();

        Cursor c = db.query( true, TABLE, colums, _ID + "=" + id, null, null, null, null, null);

        if (c != null) {
            c.moveToFirst();
        }
        return c;
    }
    public long insert(ContentValues reg)
    {
        if (db == null)
            open();

        return db.insert(TABLE, null, reg);
    }
    public long delete(long id)
    {
        if (db == null)
            open();

        return db.delete(TABLE, "_id=" + id, null);
    }
    public long update(ContentValues reg)
    {
        long result = 0;

        if (db == null)
            open();

        if (reg.containsKey(_ID))
        {
            long id = reg.getAsLong(_ID);
            reg.remove(_ID);
            result = db.update(TABLE, reg, "_id=" + id, null);
        }
        return result;
    }

    public boolean exists(long id) throws SQLException
    {
        boolean exists ;

        if (db == null)
            open();

        Cursor c = db.query( true, TABLE, colums, _ID + "=" + id, null, null, null, null, null);
        exists = (c.getCount() > 0);
        c.close();
        return exists;
    }

    public ArrayList<Price> getPrice(String filter)
    {
        ArrayList<Price> prices = new ArrayList<Price>();
        if (db == null)
            open();

        Cursor c = db.query(true, TABLE, colums, filter, null, null, null, null, null);
        for(c.moveToFirst(); !c.isAfterLast(); c.moveToNext())
        {
            prices.add(Price.cursorToPrice(context, c));
        }

        c.close();

        return prices;
    }



}