package com.lesson.vadim.prosqlite;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;


import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class PriceActivity extends ListActivity{

    public static final String MODE = "modo" ;
    public static final int VISIBLY = 551 ;
    public static final int CREATE = 552 ;
    public static final int EDIT = 553 ;
    public static final int DELETE = 554 ;

    private PriceDbAdapter dbAdapter;
    private PriceAdapter priceAdapter;
    private ListView list;
    Button btn;

    private String filter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_price);

        list = (ListView) findViewById(android.R.id.list);

        btn = (Button)findViewById(R.id.btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Intent intent = new Intent(PriceActivity.this, OrderActivity.class);
                intent.putExtra(MODE, CREATE);
                startActivityForResult(intent, CREATE);
            }
        });

        dbAdapter = new PriceDbAdapter(this);
        dbAdapter.open();

        consult();
        registerForContextMenu(this.getListView());
    }

    private void consult()
    {
        priceAdapter = new PriceAdapter(this, dbAdapter.getPrice(filter));
        list.setAdapter(priceAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    private void visibly(long id)
    {
        Intent i = new Intent(PriceActivity.this, OrderActivity.class);
        i.putExtra(MODE, VISIBLY);
        i.putExtra(PriceDbAdapter._ID, id);
        startActivityForResult(i, VISIBLY);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id)
    {
        super.onListItemClick(l, v, position, id);
        visibly(id);
    }

// Add new order
    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.menu_creat:
                Intent i = new Intent(PriceActivity.this, OrderActivity.class);
                i.putExtra(MODE, CREATE);
                startActivityForResult(i, CREATE);
                return true;
        }
        return super.onMenuItemSelected(featureId, item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        switch(requestCode)
        {
            case CREATE:
                if (resultCode == RESULT_OK)
                    consult();
            case VISIBLY:
                if (resultCode == RESULT_OK)
                    consult();
            default:
                super.onActivityResult(requestCode, resultCode, data);
        }
    }

// Delete dialog Yes/No
    private void order(final long id)
    {
        AlertDialog.Builder dialogEliminar = new AlertDialog.Builder(this);

        dialogEliminar.setIcon(android.R.drawable.ic_dialog_alert);
        dialogEliminar.setTitle(getResources().getString(R.string.remove));
        dialogEliminar.setMessage(getResources().getString(R.string.are_you_sure));
        dialogEliminar.setCancelable(false);

        dialogEliminar.setPositiveButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int boton) {
                dbAdapter.delete(id);
                Toast.makeText(PriceActivity.this, "Order deleted!", Toast.LENGTH_SHORT).show();
                consult();
            }
        });

        dialogEliminar.setNegativeButton(android.R.string.no, null);

        dialogEliminar.show();
    }

// By clicking on the order menu appears with a list of possible actions
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle(priceAdapter.getItem(((AdapterContextMenuInfo) menuInfo).position).getNumber());
        menu.add(Menu.NONE, VISIBLY, Menu.NONE, R.string.menu_visibly);
        menu.add(Menu.NONE, EDIT, Menu.NONE, R.string.menu_edit);
        menu.add(Menu.NONE, DELETE, Menu.NONE, R.string.menu_delete);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();

        switch(item.getItemId())
        {
            case DELETE:
                order(info.id);
                return true;
            case VISIBLY:
                visibly(info.id);
                return true;
            case EDIT:
                Intent intent = new Intent(PriceActivity.this, OrderActivity.class);
                intent.putExtra(MODE, EDIT);
                intent.putExtra(PriceDbAdapter._ID, info.id);
                startActivityForResult(intent, EDIT);
                return true;
        }
        return super.onContextItemSelected(item);
    }
}