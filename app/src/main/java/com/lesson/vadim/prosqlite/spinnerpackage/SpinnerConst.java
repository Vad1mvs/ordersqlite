package com.lesson.vadim.prosqlite.spinnerpackage;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

public class SpinnerConst {

    private Context context;

    private Long id;
    private String spNumber;

    public SpinnerConst(Context context)
    {
        this.context = context;
    }

    public SpinnerConst(Context context, Long id, String spNumber) {
        this.context = context;
        this.id = id;
        this.spNumber = spNumber;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSpNumber() {
        return spNumber;
    }

    public void setSpNumber(String spNumber) {
        this.spNumber = spNumber;
    }



    public static SpinnerConst find(Context context, long id)
    {
        SpinnerDbAdapter dbAdapter = new SpinnerDbAdapter(context);

        Cursor c = dbAdapter.getRegistro(id);

        SpinnerConst spinnerConst = SpinnerConst.cursorToSpinnerConst(context, c);

        c.close();

        return spinnerConst;
    }

    public static ArrayList<SpinnerConst> getAll(Context context, String filter)
    {
        SpinnerDbAdapter dbAdapter = new SpinnerDbAdapter(context);

        return dbAdapter.getSpinner(filter) ;

    }


    public static SpinnerConst cursorToSpinnerConst(Context context, Cursor c)
    {
        SpinnerConst spinner = null;

        if (c != null)
        {
            spinner = new SpinnerConst(context);

            spinner.setId(c.getLong(c.getColumnIndex(SpinnerDbAdapter.C_ID)));
            spinner.setSpNumber(c.getString(c.getColumnIndex(SpinnerDbAdapter.C_NUMBER)));
        }

        return spinner ;
    }



    public static SpinnerConst cursorToSpinnerConst2(Context context, Cursor c)
    {
        SpinnerConst spinner = null;

        if (c != null)
        {
            spinner = new SpinnerConst(context);
            spinner.setId(c.getLong(c.getColumnIndex(SpinnerDbAdapter2.C_ID2)));
            spinner.setSpNumber(c.getString(c.getColumnIndex(SpinnerDbAdapter2.C_NUMBER2)));
        }

        return spinner ;
    }


    public static SpinnerConst find2(Context context, long id)
    {
        SpinnerDbAdapter2 dbAdapter = new SpinnerDbAdapter2(context);

        Cursor c = dbAdapter.getRegistro2(id);

        SpinnerConst spinnerConst = SpinnerConst.cursorToSpinnerConst2(context, c);

        c.close();

        return spinnerConst;
    }

    public static ArrayList<SpinnerConst> getAll2(Context context, String filter)
    {
        SpinnerDbAdapter2 dbAdapter = new SpinnerDbAdapter2(context);

        return dbAdapter.getSpinner2(filter) ;

    }

}
