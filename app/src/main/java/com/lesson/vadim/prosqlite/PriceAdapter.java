package com.lesson.vadim.prosqlite;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import com.lesson.vadim.prosqlite.spinnerpackage.SpinnerAdapter;

import java.util.ArrayList;


public class PriceAdapter extends ArrayAdapter<Price>
{
    private SpinnerAdapter spinnerAdapter;
    private static class ViewHolder {
        TextView tvOrder, tvDate, tvNum, tvSum;
    }

    public PriceAdapter(Context context, ArrayList<Price> prices) {
        super(context, android.R.layout.simple_dropdown_item_1line, prices);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Price price = getItem(position);
        ViewHolder viewHolder;

        if (convertView == null)
        {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.item, parent, false);
            viewHolder.tvOrder = (TextView) convertView.findViewById(R.id.tvOrder);
            viewHolder.tvDate = (TextView) convertView.findViewById(R.id.tvDate);
            viewHolder.tvSum = (TextView) convertView.findViewById(R.id.tvSum);

            viewHolder.tvNum = (TextView) convertView.findViewById(R.id.tvNum);
            viewHolder.tvNum.setText((position + 1)+ ". ");
            convertView.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolder) convertView.getTag();
        }


        viewHolder.tvOrder.setText(price.getNumber());
        viewHolder.tvDate.setText(price.getDate());
        viewHolder.tvSum.setText(price.getCount());
        return convertView;
    }

    @Override
    public long getItemId(int position)
    {
        return getItem(position).getId();
    }


}