package com.lesson.vadim.prosqlite.spinnerpackage;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.lesson.vadim.prosqlite.PriceDbHelper;

import java.util.ArrayList;

/**
 * Created by Vadim on 02.12.2015.
 */
public class SpinnerDbAdapter2 {

    public static final String C_TABLE2 = "SPINNER2" ;

    public static final String C_ID2 = "_id";
    public static final String C_NUMBER2 = "spinner_num_2";

    private Context context;
    private PriceDbHelper dbHelper;
    private SQLiteDatabase db;

    private String[] column = new String[]{
            C_ID2,
            C_NUMBER2
    } ;

    public SpinnerDbAdapter2(Context context)
    {
        this.context = context;
    }

    public SpinnerDbAdapter2 abrir() throws SQLException
    {
        dbHelper = new PriceDbHelper(context);
        db = dbHelper.getWritableDatabase();
        return this;
    }

    public void cerrar()
    {
        dbHelper.close();
    }
    public Cursor getRegistro2(long id) throws SQLException
    {
        if (db == null)
            abrir();

        Cursor c = db.query( true, C_TABLE2, column, C_ID2 + "=" + id, null, null, null, null, null);
        if (c != null) {
            c.moveToFirst();
        }
        return c;
    }

    public ArrayList<SpinnerConst> getSpinner2(String filter)
    {
        ArrayList<SpinnerConst> spinnerConsts = new ArrayList<SpinnerConst>();

        if (db == null)
            abrir();

        Cursor c = db.query(true, C_TABLE2, column, filter, null, null, null, null, null);

        for(c.moveToFirst(); !c.isAfterLast(); c.moveToNext())
        {
            spinnerConsts.add(SpinnerConst.cursorToSpinnerConst2(context, c));
        }

        c.close();

        return spinnerConsts;
    }

}
