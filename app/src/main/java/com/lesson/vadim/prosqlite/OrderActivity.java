package com.lesson.vadim.prosqlite;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.lesson.vadim.prosqlite.spinnerpackage.SpinnerAdapter;
import com.lesson.vadim.prosqlite.spinnerpackage.SpinnerConst;
import com.lesson.vadim.prosqlite.spinnerpackage.SpinnerDbAdapter;
import com.lesson.vadim.prosqlite.spinnerpackage.SpinnerDbAdapter2;

import java.util.Calendar;

public class OrderActivity extends Activity implements View.OnClickListener {

    private SpinnerDbAdapter dbAdapterSituacion ;
    private SpinnerDbAdapter2 dbAdapterSituacion2 ;
    private SpinnerAdapter spinnerAdapter;

    private int mode;

    private long id ;
    private Price price = new Price(this);

    private EditText number, color, note, phone, email, numTable, numChair;
    private Spinner spinnerTable;
    private Spinner spinnerChair;

    private Button btnSave, btnCancel, btnDelete, btnEdit;

    Button btnDate;
    TextView tvDate, tvCount;
    private static final int Date_id = 0;
    LinearLayout buttons, buttons2;
    String date;
    String buyChair;
    int t, c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order);

        Intent intent = getIntent();
        Bundle extra = intent.getExtras();

        if (extra == null) return;

        number = (EditText) findViewById(R.id.number);
        color = (EditText) findViewById(R.id.color);
        note = (EditText) findViewById(R.id.note);
        phone = (EditText) findViewById(R.id.phone);
        email = (EditText) findViewById(R.id.email);
        numTable = (EditText) findViewById(R.id.numTable);
        numChair = (EditText) findViewById(R.id.numChair);
        spinnerTable = (Spinner) findViewById(R.id.spinnerTable);
        spinnerChair = (Spinner) findViewById(R.id.spinnerChair);
        btnSave = (Button) findViewById(R.id.btnSave);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        btnEdit = (Button) findViewById(R.id.btnEdit);
        btnDelete = (Button) findViewById(R.id.btnDelete);
        btnDate = (Button) findViewById(R.id.btnDate);
        btnSave.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        btnDelete.setOnClickListener(this);
        btnEdit.setOnClickListener(this);
        btnDate.setOnClickListener(this);
        tvDate = (TextView) findViewById(R.id.tvDate);
        tvCount = (TextView) findViewById(R.id.tvCount);

        spinnerAdapter = new SpinnerAdapter(this, SpinnerConst.getAll(this, null));
        spinnerTable.setAdapter(spinnerAdapter);

        spinnerAdapter = new SpinnerAdapter(this, SpinnerConst.getAll2(this, null));
        spinnerChair.setAdapter(spinnerAdapter);

        spinnerTable.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int tabNum = Integer.parseInt(numTable.getText().toString());

                if (i == 0) {
                    t = 3200 * tabNum;
                }
                if (i == 1) {
                    t = 3400 * tabNum;
                }
                if (i == 2) {
                    t = 2200 * tabNum;
                }
                if (i == 3) {
                    t = 1200 * tabNum;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerChair.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                int chaNum = Integer.parseInt(numChair.getText().toString());
                if (i == 0) {
                    c = 870 * chaNum;
                }
                if (i == 1) {
                    c = 870 * chaNum;
                }
                if (i == 2) {
                    c = 870 * chaNum;
                }
                if (i == 3) {
                    c = 870 * chaNum;
                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        buyChair = String.valueOf(c + t);

        if (extra.containsKey(PriceDbAdapter._ID)) {
            id = extra.getLong(PriceDbAdapter._ID);
            consult(id);
        }
        setMode(extra.getInt(PriceActivity.MODE));
    }

    private void setMode(int m)
    {
        this.mode = m ;

        if (mode == PriceActivity.VISIBLY)
        {
            this.setTitle(number.getText().toString());
            this.setEdition(false);

        }
        else if (mode == PriceActivity.CREATE)
        {
            this.setTitle("New");
            this.setEdition(true);
        }
        else if (mode == PriceActivity.EDIT)
        {
            this.setTitle("Edit");
            this.setEdition(true);
        }
    }

    private void consult(long id)
    {
        price = Price.find(this, id);

        number.setText(price.getNumber());
        color.setText(price.getColor());
        note.setText(price.getNote());
        phone.setText(price.getPhone());
        email.setText(price.getEmail());
        numTable.setText(price.getQuantity1());
        numChair.setText(price.getQuantity2());
        spinnerTable.setSelection(spinnerAdapter.getPositionById(price.getSpinnerId()));
        spinnerChair.setSelection(spinnerAdapter.getPositionById(price.getSpinnerId_2()));
        tvDate.setText(price.getDate());

        tvCount.setText(price.getCount());
    }

    private void setEdition(boolean opcion) {
        number.setEnabled(opcion);
        color.setEnabled(opcion);
        note.setEnabled(opcion);
        phone.setEnabled(opcion);
        email.setEnabled(opcion);
        numTable.setEnabled(opcion);
        numChair.setEnabled(opcion);
        spinnerTable.setEnabled(opcion);
        spinnerChair.setEnabled(opcion);
        tvDate.setEnabled(opcion);

        tvCount.setEnabled(opcion);

        buttons = (LinearLayout) findViewById(R.id.buttons);
        buttons2 = (LinearLayout) findViewById(R.id.buttons2);

        if (opcion) {
            buttons.setVisibility(View.VISIBLE);
            buttons2.setVisibility(View.GONE);
            tvDate.setVisibility(View.INVISIBLE);
            btnDate.setVisibility(View.VISIBLE);

        } else {
            buttons.setVisibility(View.GONE);
            buttons2.setVisibility(View.VISIBLE);
            tvDate.setVisibility(View.VISIBLE);
            btnDate.setVisibility(View.GONE);

        }
    }

    private void save()
    {
        price.setNumber(number.getText().toString());
        price.setColor(color.getText().toString());
        price.setNote(note.getText().toString());
        price.setPhone(phone.getText().toString());
        price.setEmail(email.getText().toString());
        price.setQuantity1(numTable.getText().toString());
        price.setQuantity2(numChair.getText().toString());

        price.setSpinnerId(spinnerTable.getSelectedItemId());
        price.setSpinnerId_2(spinnerChair.getSelectedItemId());
        price.setDate(date);

        buyChair = String.valueOf(c + t);
        price.setCount(buyChair);

        price.save();

        if (mode == PriceActivity.CREATE) {
            Toast.makeText(OrderActivity.this, "Created!", Toast.LENGTH_SHORT).show();
        } else if (mode == PriceActivity.EDIT) {
            Toast.makeText(OrderActivity.this, "Edit", Toast.LENGTH_SHORT).show();
        }
        setResult(RESULT_OK);
        finish();
    }

    private void cancel()
    {
        setResult(RESULT_CANCELED, null);
        finish();
    }

// OnClickListener all buttons
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnSave:
                save();
                break;
            case R.id.btnCancel:
                cancel();
                break;
            case R.id.btnDelete:
                dialogMethod(id);
                break;
            case R.id.btnEdit:
                setMode(PriceActivity.EDIT);
                break;
            case R.id.btnDate:
                showDialog(Date_id);
                break;
            default:
                break;
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        menu.clear();

        if (mode == PriceActivity.VISIBLY)
            getMenuInflater().inflate(R.menu.menu_order, menu);

        else
            getMenuInflater().inflate(R.menu.price_edit_menu, menu);

        return true;
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {

        switch (item.getItemId())
        {
            case R.id.menu_delete:
                dialogMethod(id);
                return true;
            case R.id.menu_cancel:
                cancel();
                return true;
            case R.id.menu_save:
                save();
                return true;
            case R.id.menu_edit:
                setMode(PriceActivity.EDIT);
                return true;
        }

        return super.onMenuItemSelected(featureId, item);
    }

    // Date listener Dialog
    protected Dialog onCreateDialog(int id) {
        Calendar c = Calendar.getInstance();
        // From calender get the year, month, day
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        switch (id) {
            case Date_id:
                // Open the date dialog
                return new DatePickerDialog(OrderActivity.this, date_listener, year, month, day);
        }
        return null;
    }
    DatePickerDialog.OnDateSetListener date_listener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int day) {
            // store the data in one string and set it to text
            date = "Date: "+ String.valueOf(day) + ". " + String.valueOf(month + 1) + ". " + String.valueOf(year);
        }
    };


    private void dialogMethod(final long id)
    {
        AlertDialog.Builder dialogEliminar = new AlertDialog.Builder(this);
        dialogEliminar.setIcon(android.R.drawable.ic_dialog_alert);
        dialogEliminar.setTitle(getResources().getString(R.string.delete_order_title));
        dialogEliminar.setMessage(getResources().getString(R.string.delete_order));
        dialogEliminar.setCancelable(false);

        dialogEliminar.setPositiveButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int buttons) {
                price.delete();
                Toast.makeText(OrderActivity.this, "Order deleted!", Toast.LENGTH_SHORT).show();
                setResult(RESULT_OK);
                finish();
            }
        });

        dialogEliminar.setNegativeButton(android.R.string.no, null);
        dialogEliminar.show();

    }


}